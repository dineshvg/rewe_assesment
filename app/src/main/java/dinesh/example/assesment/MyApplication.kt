package dinesh.example.assesment

import android.app.Application
import android.support.annotation.Nullable
import android.util.Log
import android.util.Log.INFO
import dinesh.example.test.BuildConfig
import timber.log.Timber
import timber.log.Timber.DebugTree

class MyApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        //startKoin(this, listOf(comicModule))
        setupTimber()
    }

    private fun setupTimber() {
        if (BuildConfig.DEBUG) {
            Timber.plant(DebugTree())
        } else {
            Timber.plant(CrashReportingTree())
        }
    }

    open class CrashReportingTree : Timber.Tree() {

        override fun log(priority: Int, tag: String?, message: String, t: Throwable?) {}

        fun isLoggable(priority: Int, @Nullable tag: String): Boolean {
            return priority >= INFO
        }

        protected fun log(priority: Int, tag: String, t: Throwable?, message: String) {
            if (priority == Log.VERBOSE || priority == Log.DEBUG) {
                return
            }

            FakeCrashLibrary.log(priority, tag, message)

            if (t != null) {
                if (priority == Log.ERROR) {
                    FakeCrashLibrary.logError(t)
                } else if (priority == Log.WARN) {
                    FakeCrashLibrary.logWarning(t)
                }
            }
        }
    }


}