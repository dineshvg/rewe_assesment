package dinesh.example.assesment.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
class ComicVolume : Parcelable {
    var error: String? = null
    var status_code: Int? = null
    val results: List<ComicVolumeItem>? = null

}

@Parcelize
class ComicVolumeItem : Parcelable {
    val name: String? = null
    val image: ComicImageItem? = null
}

@Parcelize
class ComicImageItem : Parcelable {
    val medium_url: String? = null
    val screen_large_url: String? = null
    val original_url: String? = null
}

@Parcelize
class VideoVolume : Parcelable {
    val error: String? = null
    val status_code: Int? = null
    val results: List<VideoVolumeItem>? = null
}

@Parcelize
class VideoVolumeItem : Parcelable {
    val low_url: String? = null
    val name: String? = null
    val user: String? = null
}