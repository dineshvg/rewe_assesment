package dinesh.example.assesment.model

class VolumeMissingException(message: String) : Exception(message)