package dinesh.example.assesment.presentation

import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import dinesh.example.assesment.presentation.comics.ComicsFragment
import dinesh.example.assesment.presentation.videos.VideoFragment
import dinesh.example.test.R
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.navigation_comics -> {
                replaceFragmentInHomeContent(ComicsFragment.newInstance())
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
        replaceFragmentInHomeContent(ComicsFragment.newInstance())
    }

    private fun replaceFragmentInHomeContent(f: Fragment) {
        content.tag = f::class.simpleName
        supportFragmentManager.beginTransaction().replace(R.id.content, f).commit()
        supportFragmentManager.executePendingTransactions()
    }


}
