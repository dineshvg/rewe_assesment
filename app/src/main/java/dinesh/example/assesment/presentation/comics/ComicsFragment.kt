package dinesh.example.assesment.presentation.comics

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.squareup.picasso.Picasso
import dinesh.example.assesment.api.ApiResult
import dinesh.example.assesment.api.RestCaller
import dinesh.example.assesment.model.ComicVolume
import dinesh.example.assesment.model.ComicVolumeItem
import dinesh.example.assesment.model.VideoVolume
import dinesh.example.test.R
import kotlinx.android.synthetic.main.comics_fragment.*
import org.jetbrains.anko.customView
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.imageView
import org.jetbrains.anko.support.v4.alert
import org.jetbrains.anko.uiThread
import timber.log.Timber

class ComicsFragment : Fragment() {

    companion object {
        fun newInstance() = ComicsFragment()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.comics_fragment, container, false)
    }

    override fun onStart() {
        super.onStart()
        Timber.d("ComicsFragment")
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        comic_item_search.setOnClickListener {
            comic_item_name.text.toString().let {
                if (!it.isNullOrBlank() && !it.isNullOrEmpty()) {
                    loadRecyclerView(it)
                }
            }

        }
    }

    private fun loadRecyclerView(comicName: String) {
        doAsync {
            RestCaller().callForComicVolumes(comicName, 10, object : ApiResult {

                override fun onError(e: Exception) {
                    Timber.e("error loading comics ${e.message}")
                }

                override fun onComicVolumeSuccess(comicVolume: ComicVolume) {
                    if (comicVolume.status_code == 1) {
                        val resultList = comicVolume.results
                        uiThread {
                            val adapter = ComicAdapter(context!!, resultList)
                            comic_list_recycler_view.adapter = adapter
                            comic_list_recycler_view.layoutManager = LinearLayoutManager(context)

                            adapter.alertCallback = object : AnkoAlertCallback {
                                override fun onAlert(result: ComicVolumeItem) {
                                    alert {
                                        customView {
                                            Picasso.get().load(result.image?.original_url)
                                                .into(imageView())
                                        }
                                        title = result.name!!
                                        negativeButton("Close") { dialog -> dialog.dismiss() }
                                        show()
                                    }
                                }
                            }
                        }

                    }
                }

                override fun onVideoSuccess(videoVolume: VideoVolume) {
                    //nothing to do here
                }

                override fun onAPIFail() {
                    Timber.e("error loading comics. Api fails")
                }

            })
        }

    }

}
