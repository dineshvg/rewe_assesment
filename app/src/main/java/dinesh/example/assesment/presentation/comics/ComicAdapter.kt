package dinesh.example.assesment.presentation.comics

import android.content.Context
import android.net.Uri
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso
import dinesh.example.assesment.model.ComicVolumeItem
import dinesh.example.test.R
import kotlinx.android.synthetic.main.list_row_comic.view.*
import timber.log.Timber


class ComicAdapter(private val context: Context, private val resultList: List<ComicVolumeItem>?) :
    RecyclerView.Adapter<ComicAdapter.ComicViewHolder>() {

    lateinit var alertCallback: AnkoAlertCallback

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): ComicAdapter.ComicViewHolder {
        val view = LayoutInflater.from(context).inflate(
            R.layout.list_row_comic,
            parent, false
        )

        val comicViewHolder = ComicViewHolder(view)
        comicViewHolder.alertCallback = alertCallback

        return comicViewHolder
    }

    override fun getItemCount(): Int {
        return resultList?.size!!
    }

    override fun onBindViewHolder(viewHolder: ComicAdapter.ComicViewHolder, position: Int) {
        viewHolder.bindItems(resultList?.get(position))
    }

    class ComicViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        lateinit var alertCallback: AnkoAlertCallback

        fun bindItems(result: ComicVolumeItem?) {
            itemView.comic_item_name.text = result?.name
            val posterUri = Uri.parse(result?.image?.medium_url).buildUpon().build()
            itemView.image_loading_progress.visibility = View.VISIBLE
            Picasso.get().load(posterUri.toString())
                .into(itemView.thumb_image, object : Callback {
                    override fun onSuccess() {
                        itemView.image_loading_progress.visibility = View.GONE
                    }

                    override fun onError(e: Exception?) {
                        itemView.image_loading_progress.visibility = View.GONE
                    }
                })
            itemView.setOnClickListener {
                Timber.d("alert callback called")
                alertCallback.onAlert(result!!)
            }
        }
    }


}