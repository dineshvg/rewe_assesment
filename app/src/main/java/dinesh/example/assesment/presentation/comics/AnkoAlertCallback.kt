package dinesh.example.assesment.presentation.comics

import dinesh.example.assesment.model.ComicVolumeItem

interface AnkoAlertCallback {
    fun onAlert(result: ComicVolumeItem)
}