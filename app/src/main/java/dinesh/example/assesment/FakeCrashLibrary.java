package dinesh.example.assesment;

public class FakeCrashLibrary {

    private FakeCrashLibrary() {
        throw new AssertionError("No instances.");
    }

    public static void log(int priority, String tag, String message) {
    }

    public static void logWarning(Throwable t) {
    }

    public static void logError(Throwable t) {
    }
}
