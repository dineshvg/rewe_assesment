package dinesh.example.assesment.api

import dinesh.example.assesment.api.Constants.Companion.APO_DETAIL
import dinesh.example.assesment.api.Constants.Companion.IMAGE
import dinesh.example.assesment.api.Constants.Companion.JSON
import dinesh.example.assesment.api.Constants.Companion.KEY
import dinesh.example.assesment.api.Constants.Companion.MAX_LIMIT
import dinesh.example.assesment.api.Constants.Companion.MIN_LIMIT
import dinesh.example.assesment.api.Constants.Companion.NAME
import dinesh.example.assesment.api.Constants.Companion.PUBLISHER
import dinesh.example.assesment.api.Constants.Companion.USER
import dinesh.example.assesment.api.Constants.Companion.VIDEO_RES
import dinesh.example.assesment.api.Constants.Companion.VOLUME
import dinesh.example.assesment.model.ComicVolume
import dinesh.example.assesment.model.VideoVolume
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface Communicator {//webservice

    /**
     * Get a list of 10 comics from one page
     * the json holds it's name, publisher and an image
     */
    @GET("/api/search?format=$JSON&field_list=$NAME,$IMAGE,$PUBLISHER,$APO_DETAIL&page=$MIN_LIMIT&resources=$VOLUME&api_key=$KEY")
    fun retrieveComicVolume(@Query("query") query: String, @Query("limit") limit: Int): Call<ComicVolume>

    /**
     * Get a list of 10 videos
     * the json holds the name, the user of the video and a low resolution video url of it
     */
    @GET("/api/videos/?api_key=$KEY&format=$JSON&limit=$MAX_LIMIT&field_list=$VIDEO_RES,$NAME,$USER&sort=field:asc")
    fun retrieveVideoVolumes(@Query("query") query: String): Call<VideoVolume>
}