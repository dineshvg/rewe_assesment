package dinesh.example.assesment.api

import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response
import retrofit2.Retrofit
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory
import java.io.IOException
import java.util.concurrent.TimeUnit

/**
 * Creates a valid retrofit object which can convert the response into one of the entities
 */
class ServiceNetwork private constructor() : Interceptor {//APIServiceNetwork

    private val logger: okhttp3.logging.HttpLoggingInterceptor
        get() {
            val logging = okhttp3.logging.HttpLoggingInterceptor()
            logging.level = okhttp3.logging.HttpLoggingInterceptor.Level.HEADERS
            logging.level = okhttp3.logging.HttpLoggingInterceptor.Level.BASIC
            logging.level = okhttp3.logging.HttpLoggingInterceptor.Level.BODY
            return logging
        }

    fun getNetworkService(url: String): Communicator {
        return createRetrofitInstance(url).create(Communicator::class.java)
    }

    private fun createRetrofitInstance(url: String): Retrofit {
        val okHttpClient = OkHttpClient().newBuilder()
        okHttpClient.addInterceptor(this)
        okHttpClient.addInterceptor(logger)
        retrofit.baseUrl(url)
            .addConverterFactory(MoshiConverterFactory.create())
            .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
            .client(
                okHttpClient.connectTimeout(
                    60,
                    TimeUnit.SECONDS
                ).writeTimeout(
                    60,
                    TimeUnit.SECONDS
                ).readTimeout(
                    60,
                    TimeUnit.SECONDS
                ).build()
            )
        return retrofit.build()
    }

    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request()
        val builder: Request.Builder
        builder = request.newBuilder()
        val nextRequest = builder.build()
        return chain.proceed(nextRequest)
    }

    companion object {
        private val retrofit = Retrofit.Builder()

        fun getInstance(): ServiceNetwork {
            return ServiceNetwork()
        }
    }
}