package dinesh.example.assesment.api

/**
 * Created by dineshvg on 22.02.19 for Example in dinesh.example.test.api.
 * 13: 20
 */
class Constants {

    companion object {
        const val KEY = "62bb0272a8b6de67d6c4f75b009a359797671113"
        const val BASE_URL = "http://www.comicvine.com"
        const val JSON = "json"
        const val NAME = "name"
        const val IMAGE = "image"
        const val PUBLISHER = "publisher"
        const val APO_DETAIL = "api_detail_url"
        const val USER = "user"
        const val VIDEO_RES = "low_url"
        const val VOLUME = "volume"
        const val MAX_LIMIT = "10"
        const val MIN_LIMIT = "1"
    }
}