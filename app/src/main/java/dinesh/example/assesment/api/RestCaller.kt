package dinesh.example.assesment.api

import android.util.Log
import dinesh.example.assesment.model.ComicVolume
import dinesh.example.assesment.model.VideoVolume
import dinesh.example.assesment.model.VolumeMissingException
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * The @RestCaller calls the serviceNetwork with a valid retrofit object and enqueues the response wrapped in
 * the APIResult callback
 */
class RestCaller {//ApiProvider

    private val TAG = RestCaller::class.simpleName

    private val serviceNetwork = ServiceNetwork.getInstance()

    fun callForComicVolumes(comicVolumeName: String, numberOfVolumes: Int, apiResult: ApiResult) {
        try {
            serviceNetwork.getNetworkService(Constants.BASE_URL)
                .retrieveComicVolume(comicVolumeName, numberOfVolumes)
                .enqueue(object : Callback<ComicVolume> {

                    override fun onFailure(call: Call<ComicVolume>, t: Throwable) {
                        Log.e(TAG, Log.getStackTraceString(t))
                        apiResult.onAPIFail()
                    }

                    override fun onResponse(call: Call<ComicVolume>, response: Response<ComicVolume>) {
                        Log.i(TAG, "${response.body()}")
                        if (response.isSuccessful) {
                            val comicVolume = response.body()
                            //make successful call when the comic volume is not empty
                            apiResult.onComicVolumeSuccess(
                                comicVolume ?: throw VolumeMissingException("Volume does not exist")
                            )
                        }
                    }

                })
        } catch (e: Exception) {
            Log.e(TAG, Log.getStackTraceString(e))
            apiResult.onError(e)
        }
    }

    fun callForVideoVolumes(videoVolumeName: String, apiResult: ApiResult) {
        try {
            serviceNetwork.getNetworkService(Constants.BASE_URL)
                .retrieveVideoVolumes(videoVolumeName)
                .enqueue(object : Callback<VideoVolume> {

                    override fun onFailure(call: Call<VideoVolume>, t: Throwable) {
                        Log.e(TAG, Log.getStackTraceString(t))
                        apiResult.onAPIFail()
                    }

                    override fun onResponse(call: Call<VideoVolume>, response: Response<VideoVolume>) {
                        Log.i(TAG, "${response.body()}")
                        if (response.isSuccessful) {
                            val videoVolume = response.body()
                            apiResult.onVideoSuccess(
                                videoVolume ?: throw VolumeMissingException("Volume does not exist")
                            )
                        }
                    }

                })
        } catch (e: java.lang.Exception) {
            Log.e(TAG, Log.getStackTraceString(e))
            apiResult.onError(e)
        }
    }

}