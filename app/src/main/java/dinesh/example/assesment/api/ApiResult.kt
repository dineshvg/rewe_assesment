package dinesh.example.assesment.api

import dinesh.example.assesment.model.ComicVolume
import dinesh.example.assesment.model.VideoVolume

/**
 * API callback to the view from the api
 */
interface ApiResult {
    fun onError(e: Exception)

    fun onComicVolumeSuccess(comicVolume: ComicVolume)

    fun onVideoSuccess(videoVolume: VideoVolume)

    fun onAPIFail()
}