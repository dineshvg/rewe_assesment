package dinesh.example.assesment.api

import okhttp3.*
import java.io.IOException

/**
 * Test API response with basic OkHttp
 */
class ExampleApiService {

    private val client = OkHttpClient()

    val example =
        "https://comicvine.gamespot.com/api/publishers/?api_key=75d580a0593b7320727309feb6309f62def786cd&format=json&limit=10&page=1&query=marvel&field_list=name,deck&sort=field:asc"

    internal fun fetchAComic() {

        val request = Request.Builder()
            .url(example)
            .build()

        try {
            client.newCall(request).enqueue(object : Callback {
                override fun onFailure(call: Call, e: IOException) {}
                override fun onResponse(call: Call, response: Response) = println(response.body()?.string())
            })
        } catch (e: Exception) {
            System.out.print(e.message)
        }


    }
}